#!/bin/sh

#UNLOCK_ACC=0x9d14c4b950f56e55f190bfff1574ca18f40e428a
#FROM_ACC=0x9d14c4b950f56e55f190bfff1574ca18f40e428a
#TO_ACC=0x9d14c4b950f56e55f190bfff1574ca18f40e428a
#CONTRACT_ACC=0xD06a44C4207965C01ad504de70670561DF1B2B77

#./ethc -o mint -u 0x625C151f6021C934A0905729a9700DEAD562736b -t 0x625C151f6021C934A0905729a9700DEAD562736b -c 0x329002dD24b4bBF16a41A77Fdf8e14b7ad550a6d -p 123 -f ./nau.json -a 10000
#gdb --args ./ethc -o mint -u $UNLOCK_ACC -f $FROM_ACC -t $TO_ACC -a $CONTRACT_ACC -p 123 -c ./nau.json -s 10000
valgrind --log-file=$1.log --leak-check=full --show-reachable=yes --track-origins=yes $1 $2 $3 $4 $5 $6 $7 $8 $9
